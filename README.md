# Mastermind

Un jeu de mastermind codé en HTML / CSS / JS.

## Règles du jeu :

L'ordinateur décide d'une combinaison de couleurs aléatoire, puis le joueur fait des propositions.

Après chaque proposition du joueur, l'ordinateur lui indique le nombre de pions présents dans la combinaison mais mal placés (marqueur blanc), ainsi que le nombre de pions correctement placés (marqueur rouge).

Le joueur devant deviner la combinaison continue ainsi en proposant sur la seconde ligne une autre proposition, en prenant en compte les indications des réponses précédentes.

Il a droit à 10 propositions pour déchiffrer le code.

## Roadmap

- Intégration HTML / CSS du jeu

  - 10 lignes d'essai
  - 4 couleurs
  - bouton valider

- Dévelopement de la couche modèle

  1. Définir une combinaison de couleurs (aléatoire) que le joueur devra deviner
  2. Valider une couleur
     - si elle est présente
     - si elle bien placée
  3. Valider une combinaison entière

     - obtenir le nombre de couleurs bien placées
     - obtenir le nombre de couleurs présentes mais mal placées

  4. Savoir si le jeu est fini ou bien s'il peut continuer

  5. TODO Corriger une couleur avant de valider
  6. TODO Réinitialser le jeu

- Dévelopement de la couche vue
- Amélioration du jeu avec ajouts des options
- Dévelopement de la couche persistence