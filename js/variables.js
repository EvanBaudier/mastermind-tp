/*Authors = Evan & Lucas*/

const color = [
    "red",
    "blue",
    "green",
    "yellow",
    "darkorange",
    "darkviolet",
]

const $body = document.body;
const $layout = document.getElementById("layout");
const $results = document.getElementById("results");
const $screen = document.getElementById("screen");

let roundNb = 10;
let score = 1;
const $score = document.querySelector(".score");

let ffVictory = new Audio('sound/FFwin.mp3');
let audioVictory = new Audio('sound/win.mp4');
let audioDefeat = new Audio('sound/lose.mp4');

let $turnsLeft = document.querySelector(".turnsLeft");

let $submitBtn = document.querySelector(".btn-submit");

let $finalresult1 = document.querySelector(".result-one");
let $finalresult2 = document.querySelector(".result-two");
let $finalresult3 = document.querySelector(".result-three");
let $finalresult4 = document.querySelector(".result-four");

let $firstCheckbox = document.getElementById("one");
let $secondCheckbox = document.getElementById("two");
let $thirdCheckbox = document.getElementById("three");
let $fourthCheckbox = document.getElementById("four");

const $youWin = document.querySelector(".win");
const $youlose = document.querySelector(".lose");
const $gifMLG = document.querySelector(".gifmlg");
const $gifMindBlowing = document.querySelector(".gifMindBlowing");

const $myResult1 = document.querySelector(".cbnone1");
const $myResult2 = document.querySelector(".cbnone2");
const $myResult3 = document.querySelector(".cbnone3");
const $myResult4 = document.querySelector(".cbnone4");


const $paletteOne = document.querySelector("#one .palette");
const $paletteTwo = document.querySelector("#two .palette");
const $paletteThree = document.querySelector("#three .palette");
const $paletteFour = document.querySelector("#four .palette");


const $skinSwitch = document.getElementById("skinSwitch");
const $soundMute = document.getElementById("soundMute");
const $bot = document.getElementById("bot");