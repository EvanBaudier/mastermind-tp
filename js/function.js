/*Authors = Evan & Lucas*/
/**
 *
 * @param min
 * @param max
 * @return {*}
 */
function random (min, max) {
    return Math.trunc(Math.random() * (max - min)) + min;
}

/**
 *
 * @return {string}
 */
function randomColor() {
    return color[random(0, color.length - 1)];
}

/**
 *
 * @param checkbox
 * @param nb
 * @return {boolean}
 */
function isValide (checkbox, nb) {

    if (nb === 1) return $finalresult1.style.backgroundColor === checkbox.style.backgroundColor;
    if (nb === 2) return $finalresult2.style.backgroundColor === checkbox.style.backgroundColor;
    if (nb === 3) return $finalresult3.style.backgroundColor === checkbox.style.backgroundColor;
    if (nb === 4) return $finalresult4.style.backgroundColor === checkbox.style.backgroundColor;

}

/**
 *
 * @param checkbox
 * @return {boolean}
 */
function isWrongPlace(checkbox) {
    return checkbox.style.backgroundColor === $finalresult1.style.backgroundColor ||
        checkbox.style.backgroundColor === $finalresult2.style.backgroundColor ||
        checkbox.style.backgroundColor === $finalresult3.style.backgroundColor ||
        checkbox.style.backgroundColor === $finalresult4.style.backgroundColor;

}

/**
 *
 */
function submit() {


    const html = `
    <div class="box">
        <div class="submitedResult">
            <div class="one checkbox"></div>
            <div class="two checkbox"></div>
            <div class="three checkbox"></div>
            <div class="four checkbox"></div>
        </div>
        <div>${score}</div>
        <div class="clue">
            <div class="one checkbox"></div>
            <div class="two checkbox"></div>
            <div class="three checkbox"></div>
            <div class="four checkbox"></div>
        </div>
    </div>    
    `;

    const $box = document.createElement("div");
    $box.classList.add("game");
    $box.innerHTML = html;

    const $checkBoxOne = $box.querySelector(".box > .submitedResult > .one");
    $checkBoxOne.style.backgroundColor = $firstCheckbox.style.backgroundColor;

    const $checkBoxTwo = $box.querySelector(".box > .submitedResult > .two");
    $checkBoxTwo.style.backgroundColor = $secondCheckbox.style.backgroundColor;

    const $checkBoxThree = $box.querySelector(".box > .submitedResult > .three");
    $checkBoxThree.style.backgroundColor = $thirdCheckbox.style.backgroundColor;

    const $checkBoxFour = $box.querySelector(".box > .submitedResult > .four");
    $checkBoxFour.style.backgroundColor = $fourthCheckbox.style.backgroundColor;


    const $valideBoxOne = $box.querySelector(".box > .clue > .one");
    if (isWrongPlace($checkBoxOne)) {
        $valideBoxOne.style.backgroundColor = 'grey';
    }
    if (isValide($checkBoxOne, 1)) {
        $myResult1.style.display = "block";
        $valideBoxOne.style.backgroundColor = '#00ff0080';
    } else {
        $firstCheckbox.style.backgroundColor = "";
    }

    const $valideBoxTwo = $box.querySelector(".box > .clue > .two");
    if (isWrongPlace($checkBoxTwo)) {
        $valideBoxTwo.style.backgroundColor = 'grey';
    }
    if (isValide($checkBoxTwo, 2)) {
        $myResult2.style.display = "block";
        $valideBoxTwo.style.backgroundColor = '#00ff0080';
    } else {
        $secondCheckbox.style.backgroundColor = "";
    }

    const $valideBoxThree = $box.querySelector(".box > .clue > .three");
    if (isWrongPlace($checkBoxThree)) {
        $valideBoxThree.style.backgroundColor = 'grey';
    }
    if (isValide($checkBoxThree, 3)) {
        $myResult3.style.display = "block";
        $valideBoxThree.style.backgroundColor = '#00ff0080';
    } else {
        $thirdCheckbox.style.backgroundColor = "";
    }

    const $valideBoxFour = $box.querySelector(".box > .clue > .four");
    if (isWrongPlace($checkBoxFour)) {
        $valideBoxFour.style.backgroundColor = 'grey';
    }
    if (isValide($checkBoxFour, 4)) {
        $myResult4.style.display = "block";
        $valideBoxFour.style.backgroundColor = '#00ff0080';
    } else {
        $fourthCheckbox.style.backgroundColor = "";
    }

    if (
        score === 1 &&
        isValide($checkBoxOne, 1) &&
        isValide($checkBoxTwo, 2) &&
        isValide($checkBoxThree, 3) &&
        isValide($checkBoxFour, 4)
    ) {
        $submitBtn.textContent = "RESET";
        ffVictory.play($gifMindBlowing.style.display = 'block');
        $youWin.style.display = "block";
        $screen.style.backgroundColor = "#00ff0080";
        $turnsLeft.style.display = "none";

    } else if (
        score > 1 &&
        isValide($checkBoxOne, 1) &&
        isValide($checkBoxTwo, 2) &&
        isValide($checkBoxThree, 3) &&
        isValide($checkBoxFour, 4)
    ) {
        $submitBtn.textContent = "RESET";
        audioVictory.play($gifMLG.style.display = 'block');
        $youWin.style.display = "block";
        $screen.style.backgroundColor = "#00ff0080";
        $turnsLeft.style.display = "none";

    } else if (
        roundNb <= 1 &&
        (
        !isValide($checkBoxOne, 1) ||
        !isValide($checkBoxTwo, 2) ||
        !isValide($checkBoxThree, 3) ||
        !isValide($checkBoxFour, 4)
        )
    ) {
        $submitBtn.textContent = "RESET";
        audioDefeat.play($youlose.style.display = "block");
        $youlose.style.display = "block";
        $screen.style.backgroundColor = "#ff63479c";
        $turnsLeft.style.display = "none";

    }

    if (roundNb > 0) $results.prepend($box);

    roundNb--;
    score++;
    $turnsLeft.textContent = "lft : " + (roundNb);

    $submitBtn.style.backgroundColor = "";

}