/*Authors = Evan & Lucas*/

//-----------------------------------------------
//            Choose color combination
//-----------------------------------------------
$finalresult1.style.backgroundColor = randomColor();
$finalresult2.style.backgroundColor = randomColor();
$finalresult3.style.backgroundColor = randomColor();
$finalresult4.style.backgroundColor = randomColor();

/*$finalresult1.style.backgroundColor = "red";
$finalresult2.style.backgroundColor = "green";
$finalresult3.style.backgroundColor = "yellow";
$finalresult4.style.backgroundColor = "darkorange";*/

//-----------------------------------------------
//                Set turns left
//-----------------------------------------------
$turnsLeft.textContent = "tnb : " + (roundNb);

//-----------------------------------------------
//                 Play button
//-----------------------------------------------
$submitBtn.addEventListener("mousedown", function(){
    if ($submitBtn.textContent === "RESET") {
        document.location.reload();
    } else if (
        $firstCheckbox.style.backgroundColor !== "" &&
        $secondCheckbox.style.backgroundColor !== "" &&
        $thirdCheckbox.style.backgroundColor !== "" &&
        $fourthCheckbox.style.backgroundColor !== ""
    ) {
        $submitBtn.textContent = "PLAY"
        submit();
        $submitBtn.style.backgroundColor = "#00ff0080";
    } else $submitBtn.style.backgroundColor = "#ff63479c";
    if ($firstCheckbox.style.backgroundColor === "")
        $firstCheckbox.style.backgroundColor = "rgba(255, 99, 71, 0.61)";
    if ($secondCheckbox.style.backgroundColor === "")
        $secondCheckbox.style.backgroundColor = "rgba(255, 99, 71, 0.61)";
    if ($thirdCheckbox.style.backgroundColor === "")
        $thirdCheckbox.style.backgroundColor = "rgba(255, 99, 71, 0.61)";
    if ($fourthCheckbox.style.backgroundColor === "")
        $fourthCheckbox.style.backgroundColor = "rgba(255, 99, 71, 0.61)";

})
$submitBtn.addEventListener("mouseup", function() {
    $submitBtn.style.backgroundColor = "";
    if ($firstCheckbox.style.backgroundColor === "rgba(255, 99, 71, 0.61)")
        $firstCheckbox.style.backgroundColor = "";
    if ($secondCheckbox.style.backgroundColor === "rgba(255, 99, 71, 0.61)")
        $secondCheckbox.style.backgroundColor = "";
    if ($thirdCheckbox.style.backgroundColor === "rgba(255, 99, 71, 0.61)")
        $thirdCheckbox.style.backgroundColor = "";
    if ($fourthCheckbox.style.backgroundColor === "rgba(255, 99, 71, 0.61)")
        $fourthCheckbox.style.backgroundColor = "";
})

//-----------------------------------------------
//                 Color palettes
//-----------------------------------------------
$firstCheckbox.addEventListener("mousedown", function() {

    if ($submitBtn.textContent !== "RESET") {

        $firstCheckbox.style.boxShadow = "inset 0 0 1px 1px #aaa";
        $secondCheckbox.style.boxShadow = "0 0";
        $thirdCheckbox.style.boxShadow = "0 0";
        $fourthCheckbox.style.boxShadow = "0 0";

        $paletteOne.style.display = "flex";
        $paletteTwo.style.display = "none";
        $paletteThree.style.display = "none";
        $paletteFour.style.display = "none";

        $paletteOne.addEventListener("mouseover", function() {
            $firstCheckbox.style.backgroundColor = "";
        })

        const $redOne = document.querySelector("#one .red");
        $redOne.addEventListener("mouseenter", function() {
            $firstCheckbox.style.backgroundColor = "red";
        })
        $redOne.addEventListener("mouseup", function () {
            $firstCheckbox.style.backgroundColor = "red";
            $paletteOne.style.display = "none";
            $firstCheckbox.style.boxShadow = "0 0";
        })

        const $greenOne = document.querySelector("#one .green");
        $greenOne.addEventListener("mouseenter", function() {
            $firstCheckbox.style.backgroundColor = "green";
        })
        $greenOne.addEventListener("mouseup", function () {
            $firstCheckbox.style.backgroundColor = "green";
            $paletteOne.style.display = "none";
            $firstCheckbox.style.boxShadow = "0 0";
        })

        const $yellowOne = document.querySelector(".yellow");
        $yellowOne.addEventListener("mouseenter", function() {
            $firstCheckbox.style.backgroundColor = "yellow";
        })
        $yellowOne.addEventListener("mouseup", function () {
            $firstCheckbox.style.backgroundColor = "yellow";
            $paletteOne.style.display = "none";
            $firstCheckbox.style.boxShadow = "0 0";
        })

        const $deeppinkOne = document.querySelector("#one .darkorange");
        $deeppinkOne.addEventListener("mouseenter", function() {
            $firstCheckbox.style.backgroundColor = "darkorange";
        })
        $deeppinkOne.addEventListener("mouseup", function () {
            $firstCheckbox.style.backgroundColor = "darkorange";
            $paletteOne.style.display = "none";
            $firstCheckbox.style.boxShadow = "0 0";
        })

        const $indigoOne = document.querySelector("#one .darkviolet");
        $indigoOne.addEventListener("mouseenter", function() {
            $firstCheckbox.style.backgroundColor = "darkviolet";
        })
        $indigoOne.addEventListener("mouseup", function () {
            $firstCheckbox.style.backgroundColor = "darkviolet";
            $paletteOne.style.display = "none";
            $firstCheckbox.style.boxShadow = "0 0";
        })

        const $blueOne = document.querySelector("#one .blue");
        $blueOne.addEventListener("mouseenter", function() {
            $firstCheckbox.style.backgroundColor = "blue";
        })
        $blueOne.addEventListener("mouseup", function () {
            $firstCheckbox.style.backgroundColor = "blue";
            $paletteOne.style.display = "none";
            $firstCheckbox.style.boxShadow = "0 0";
        })
    }
});
$secondCheckbox.addEventListener("mousedown", function() {

    if ($submitBtn.textContent !== "RESET") {

        $secondCheckbox.style.boxShadow = "inset 0 0 1px 1px #aaa";
        $firstCheckbox.style.boxShadow = "0 0";
        $thirdCheckbox.style.boxShadow = "0 0";
        $fourthCheckbox.style.boxShadow = "0 0";

        $paletteTwo.style.display = "flex";
        $paletteOne.style.display = "none";
        $paletteThree.style.display = "none";
        $paletteFour.style.display = "none";

        $paletteTwo.addEventListener("mouseover", function() {
            $secondCheckbox.style.backgroundColor = "";
        })

        const $redTwo = document.querySelector("#two .red");
        $redTwo.addEventListener("mouseenter", function() {
            $secondCheckbox.style.backgroundColor = "red";
        })
        $redTwo.addEventListener("mouseup", function () {
            $secondCheckbox.style.backgroundColor = "red";
            $paletteTwo.style.display = "none";
            $secondCheckbox.style.boxShadow = "0 0";
        })

        const $greenTwo = document.querySelector("#two .green");
        $greenTwo.addEventListener("mouseenter", function() {
            $secondCheckbox.style.backgroundColor = "green";
        })
        $greenTwo.addEventListener("mouseup", function () {
            $secondCheckbox.style.backgroundColor = "green";
            $paletteTwo.style.display = "none";
            $secondCheckbox.style.boxShadow = "0 0";
        })

        const $yellowTwo = document.querySelector("#two .yellow");
        $yellowTwo.addEventListener("mouseenter", function() {
            $secondCheckbox.style.backgroundColor = "yellow";
        })
        $yellowTwo.addEventListener("mouseup", function () {
            $secondCheckbox.style.backgroundColor = "yellow";
            $paletteTwo.style.display = "none";
            $secondCheckbox.style.boxShadow = "0 0";
        })

        const $deeppinkTwo = document.querySelector("#two .darkorange");
        $deeppinkTwo.addEventListener("mouseenter", function() {
            $secondCheckbox.style.backgroundColor = "darkorange";
        })
        $deeppinkTwo.addEventListener("mouseup", function () {
            $secondCheckbox.style.backgroundColor = "darkorange";
            $paletteTwo.style.display = "none";
            $secondCheckbox.style.boxShadow = "0 0";
        })

        const $indigoTwo = document.querySelector("#two .darkviolet");
        $indigoTwo.addEventListener("mouseenter", function() {
            $secondCheckbox.style.backgroundColor = "darkviolet";
        })
        $indigoTwo.addEventListener("mouseup", function () {
            $secondCheckbox.style.backgroundColor = "darkviolet";
            $paletteTwo.style.display = "none";
            $secondCheckbox.style.boxShadow = "0 0";
        })

        const $blueTwo = document.querySelector("#two .blue");
        $blueTwo.addEventListener("mouseenter", function() {
            $secondCheckbox.style.backgroundColor = "blue";
        })
        $blueTwo.addEventListener("mouseup", function () {
            $secondCheckbox.style.backgroundColor = "blue";
            $paletteTwo.style.display = "none";
            $secondCheckbox.style.boxShadow = "0 0";
        })
    }
});
$thirdCheckbox.addEventListener("mousedown", function() {

    if ($submitBtn.textContent !== "RESET") {

        $thirdCheckbox.style.boxShadow = "inset 0 0 1px 1px #aaa";
        $firstCheckbox.style.boxShadow = "0 0";
        $secondCheckbox.style.boxShadow = "0 0";
        $fourthCheckbox.style.boxShadow = "0 0";

        $paletteThree.style.display = "flex";
        $paletteTwo.style.display = "none";
        $paletteOne.style.display = "none";
        $paletteFour.style.display = "none";

        $paletteThree.addEventListener("mouseover", function() {
            $thirdCheckbox.style.backgroundColor = "";
        })

        const $redThree = document.querySelector("#three .red");
        $redThree.addEventListener("mouseenter", function() {
            $thirdCheckbox.style.backgroundColor = "red";
        })
        $redThree.addEventListener("mouseup", function () {
            $thirdCheckbox.style.backgroundColor = "red";
            $paletteThree.style.display = "none";
            $thirdCheckbox.style.boxShadow = "0 0";
        })

        const $greenThree = document.querySelector("#three .green");
        $greenThree.addEventListener("mouseenter", function() {
            $thirdCheckbox.style.backgroundColor = "green";
        })
        $greenThree.addEventListener("mouseup", function () {
            $thirdCheckbox.style.backgroundColor = "green";
            $paletteThree.style.display = "none";
            $thirdCheckbox.style.boxShadow = "0 0";
        })

        const $yellowThree = document.querySelector("#three .yellow");
        $yellowThree.addEventListener("mouseenter", function() {
            $thirdCheckbox.style.backgroundColor = "yellow";
        })
        $yellowThree.addEventListener("mouseup", function () {
            $thirdCheckbox.style.backgroundColor = "yellow";
            $paletteThree.style.display = "none";
            $thirdCheckbox.style.boxShadow = "0 0";
        })

        const $deeppinkThree = document.querySelector("#three .darkorange");
        $deeppinkThree.addEventListener("mouseenter", function() {
            $thirdCheckbox.style.backgroundColor = "darkorange";
        })
        $deeppinkThree.addEventListener("mouseup", function () {
            $thirdCheckbox.style.backgroundColor = "darkorange";
            $paletteThree.style.display = "none";
            $thirdCheckbox.style.boxShadow = "0 0";
        })

        const $indigoThree = document.querySelector("#three .darkviolet");
        $indigoThree.addEventListener("mouseenter", function() {
            $thirdCheckbox.style.backgroundColor = "darkviolet";
        })
        $indigoThree.addEventListener("mouseup", function () {
            $thirdCheckbox.style.backgroundColor = "darkviolet";
            $paletteThree.style.display = "none";
            $thirdCheckbox.style.boxShadow = "0 0";
        })

        const $blueThree = document.querySelector("#three .blue");
        $blueThree.addEventListener("mouseenter", function() {
            $thirdCheckbox.style.backgroundColor = "blue";
        })
        $blueThree.addEventListener("mouseup", function () {
            $thirdCheckbox.style.backgroundColor = "blue";
            $paletteThree.style.display = "none";
            $thirdCheckbox.style.boxShadow = "0 0";
        })
    }
});
$fourthCheckbox.addEventListener("mousedown", function() {

    if ($submitBtn.textContent !== "RESET") {

        $fourthCheckbox.style.boxShadow = "inset 0 0 1px 1px #aaa";
        $firstCheckbox.style.boxShadow = "0 0";
        $secondCheckbox.style.boxShadow = "0 0";
        $thirdCheckbox.style.boxShadow = "0 0";

        $paletteFour.style.display = "flex";
        $paletteTwo.style.display = "none";
        $paletteThree.style.display = "none";
        $paletteOne.style.display = "none";

        $paletteFour.addEventListener("mouseover", function() {
            $fourthCheckbox.style.backgroundColor = "";
        })

        const $redFour = document.querySelector("#four .red");
        $redFour.addEventListener("mouseenter", function() {
            $fourthCheckbox.style.backgroundColor = "red";
        })
        $redFour.addEventListener("mouseup", function () {
            $fourthCheckbox.style.backgroundColor = "red";
            $paletteFour.style.display = "none";
            $fourthCheckbox.style.boxShadow = "0 0";
        })

        const $greenFour = document.querySelector("#four .green");
        $greenFour.addEventListener("mouseenter", function() {
            $fourthCheckbox.style.backgroundColor = "green";
        })
        $greenFour.addEventListener("mouseup", function () {
            $fourthCheckbox.style.backgroundColor = "green";
            $paletteFour.style.display = "none";
            $fourthCheckbox.style.boxShadow = "0 0";
        })

        const $yellowFour = document.querySelector("#four .yellow");
        $yellowFour.addEventListener("mouseenter", function() {
            $fourthCheckbox.style.backgroundColor = "yellow";
        })
        $yellowFour.addEventListener("mouseup", function () {
            $fourthCheckbox.style.backgroundColor = "yellow";
            $paletteFour.style.display = "none";
            $fourthCheckbox.style.boxShadow = "0 0";
        })

        const $deeppinkFour = document.querySelector("#four .darkorange");
        $deeppinkFour.addEventListener("mouseenter", function() {
            $fourthCheckbox.style.backgroundColor = "darkorange";
        })
        $deeppinkFour.addEventListener("mouseup", function () {
            $fourthCheckbox.style.backgroundColor = "darkorange";
            $paletteFour.style.display = "none";
            $fourthCheckbox.style.boxShadow = "0 0";
        })

        const $indigoFour = document.querySelector("#four .darkviolet");
        $indigoFour.addEventListener("mouseenter", function() {
            $fourthCheckbox.style.backgroundColor = "darkviolet";
        })
        $indigoFour.addEventListener("mouseup", function () {
            $fourthCheckbox.style.backgroundColor = "darkviolet";
            $paletteFour.style.display = "none";
            $fourthCheckbox.style.boxShadow = "0 0";
        })

        const $blueFour = document.querySelector("#four .blue");
        $blueFour.addEventListener("mouseenter", function() {
            $fourthCheckbox.style.backgroundColor = "blue";
        })
        $blueFour.addEventListener("mouseup", function () {
            $fourthCheckbox.style.backgroundColor = "blue";
            $paletteFour.style.display = "none";
            $fourthCheckbox.style.boxShadow = "0 0";
        })
    }
});

//-----------------------------------------------
//                  Toggle skins
//-----------------------------------------------
$skinSwitch.addEventListener("click", function() {
    if ($skinSwitch.style.backgroundColor === "") {
        $skinSwitch.style.backgroundColor = "rgb(170, 170, 170)";
        $layout.style.backgroundImage = "url('img/skinNight.png')";
        $body.style.backgroundColor = "#aaa";
        $submitBtn.style.color = "#eedbff";
        $paletteOne.style.backgroundColor = "#050505";
        $paletteOne.style.boxShadow = "3px 3px 5px 1px #0d0d0d";
        $paletteTwo.style.backgroundColor = "#050505";
        $paletteTwo.style.boxShadow = "3px 3px 5px 1px #0d0d0d";
        $paletteThree.style.backgroundColor = "#050505";
        $paletteThree.style.boxShadow = "3px 3px 5px 1px #0d0d0d";
        $paletteFour.style.backgroundColor = "#050505";
        $paletteFour.style.boxShadow = "3px 3px 5px 1px #0d0d0d";
        $turnsLeft.style.color = "white";
        $layout.style.boxShadow = "3px 3px 5px 1px #0d0d0d";


    } else if ($skinSwitch.style.backgroundColor === "rgb(170, 170, 170)") {
        $skinSwitch.style.backgroundColor = "";
        $layout.style.backgroundImage = "url('img/skinDay.png')";
        $body.style.backgroundColor = "#e9e9e9";
        $submitBtn.style.color = "#f7ffe0";
        $paletteOne.style.backgroundColor = "#fafafa";
        $paletteOne.style.boxShadow = "3px 3px 5px 1px #838383";
        $paletteTwo.style.backgroundColor = "#fafafa";
        $paletteTwo.style.boxShadow = "3px 3px 5px 1px #838383";
        $paletteThree.style.backgroundColor = "#fafafa";
        $paletteThree.style.boxShadow = "3px 3px 5px 1px #838383";
        $paletteFour.style.backgroundColor = "#fafafa";
        $paletteFour.style.boxShadow = "3px 3px 5px 1px #838383";
        $turnsLeft.style.color = "black";
        $layout.style.boxShadow = "3px 3px 5px 1px #838383";

    }

});
$bot.addEventListener("click", function () {
    bot();
});

//-----------------------------------------------
//                  Toggle sound
//-----------------------------------------------
$soundMute.addEventListener("click", function() {
    if ($soundMute.style.backgroundColor === "") {
        $soundMute.style.backgroundColor = "rgb(255, 0, 0)";

    } else if ($soundMute.style.backgroundColor === "rgb(255, 0, 0)") {
        $soundMute.style.backgroundColor = "";

    }
});